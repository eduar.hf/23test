package net.people.demo.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("personService")
public class PersonServiceImpl implements PersonService{
	
	@Autowired
	private PersonRepository repository;
	
	@Override
	public List<PersonDTO> all() {
			
			List <PersonDTO> list = new ArrayList<PersonDTO>();
			List <Person> listEntity = repository.findAll();
		
			for (Person person : listEntity) {
			
				PersonDTO personDTO = new PersonDTO();
				personDTO.setNationalId(person.getNationalId());
				personDTO.setName(person.getName());
				personDTO.setLastName(person.getLastName());
				personDTO.setOriginPlanet(person.getOriginPlanet());
				personDTO.setPictureUrl(person.getPictureUrl());
				list.add(personDTO);
			}
			return list;
	}
	
	@Override
	public PersonDTO save(PersonDTO personRequest) {
		
		Person entity = new Person();
		entity.setNationalId(personRequest.getNationalId());
		entity.setName(personRequest.getName());
		entity.setLastName(personRequest.getLastName());
		entity.setAge(personRequest.getAge());
		entity.setOriginPlanet(personRequest.getOriginPlanet());
		entity.setPictureUrl(personRequest.getPictureUrl());
		repository.save(entity);
		return personRequest;
	}
	
	@Override
	public PersonDTO update(PersonDTO personRequest, String nationalId) {
		
		List <Person> listEntity = repository.findByNationalId(nationalId);
		
		if(listEntity!=null && !listEntity.isEmpty()) {
			
			Person entity = listEntity.get(0);
			
			entity.setNationalId(personRequest.getNationalId());
			entity.setName(personRequest.getName());
			entity.setLastName(personRequest.getLastName());
			entity.setAge(personRequest.getAge());
			entity.setOriginPlanet(personRequest.getOriginPlanet());
			entity.setPictureUrl(personRequest.getPictureUrl());
			repository.save(entity);
			
		}else {
			personRequest = null;
		}
		
		return personRequest;
	}
	
	@Override
	public PersonDTO getSinglePerson(PersonDTO personRequest) {
		
		PersonDTO personDTO = new PersonDTO();
		List <Person> listEntity = repository.findByNationalId(personRequest.getNationalId());
		
		if(listEntity!=null && !listEntity.isEmpty()) {
			Person personEntity = listEntity.get(0);
			personDTO.setNationalId(personEntity.getNationalId());
			personDTO.setName(personEntity.getName());
			personDTO.setLastName(personEntity.getLastName());
			personDTO.setOriginPlanet(personEntity.getOriginPlanet());
			personDTO.setPictureUrl(personEntity.getPictureUrl());
		}else {
			personDTO = null;
		}
		return personDTO;
	}
	
	@Override
	public void delete(PersonDTO personRequest) {
		
		Person personEntity = new Person();
		personEntity.setNationalId(personRequest.getNationalId());
		repository.delete(personEntity);
	}
}
