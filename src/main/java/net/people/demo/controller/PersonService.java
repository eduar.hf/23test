package net.people.demo.controller;

import java.util.List;

public interface PersonService {
	
	PersonDTO getSinglePerson(PersonDTO personRequest);
	
	List<PersonDTO> all();
	
	PersonDTO save(PersonDTO personRequest);
	
	PersonDTO update(PersonDTO personRequest, String nationalId);
	
	void delete(PersonDTO personRequest);
}
