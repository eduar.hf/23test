package net.people.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
public class PeopleController {
	
	@Autowired
	private PersonService personService;

	@GetMapping(value = "/people")
    public List <PersonDTO> people(){
    
        return personService.all();
    }
	@GetMapping("/people/{id}")
	PersonDTO people(@PathVariable String id) throws Exception{
		
		PersonDTO personRequest = new PersonDTO();
		personRequest.setNationalId(id);
		
		PersonDTO personDTO = personService.getSinglePerson(personRequest);
		if(personDTO==null) {
			
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "person not found");
		}	
		return personDTO;
	}
	@PostMapping("/people")
	PersonDTO people(@RequestBody PersonDTO newEmployee) {
		
		return personService.save(newEmployee);
	}
	@PutMapping("/people/{id}")
	PersonDTO people(@RequestBody PersonDTO newPerson, @PathVariable String id) {
		
		
		return personService.update(newPerson, id);
	}
	@DeleteMapping("/people/{id}")
	public void deletePeople(@PathVariable String id){
		
		PersonDTO personRequest = new PersonDTO();
		personRequest.setNationalId(id);
		personService.delete(personRequest);	
	}
}
