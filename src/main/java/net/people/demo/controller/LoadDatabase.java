package net.people.demo.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class LoadDatabase {

	private static final Logger log = LoggerFactory.getLogger(LoadDatabase.class);

	@Bean
	CommandLineRunner initDatabase(PersonRepository repository) {

		return args -> {
			log.info("Preloading " + repository.save(new Person("12345678-9","Hari", "Seldon", 45,"Helicon","https://your.picture.com/hari-seldon")));
//			log.info("Preloading " + repository.save(new Person("1-9","Test", "Test", 36,"Quilicura","https://your.picture.com/test-test")));
		};
	}
}
