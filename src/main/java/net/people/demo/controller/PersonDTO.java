package net.people.demo.controller;

public class PersonDTO {

	private String nationalId;
	private String name;
	private String lastName;
	private Integer age;
	private String originPlanet;
	private String pictureUrl;
	
	
	public String getNationalId() {
		return nationalId;
	}
	public void setNationalId(String nationalId) {
		this.nationalId = nationalId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public String getOriginPlanet() {
		return originPlanet;
	}
	public void setOriginPlanet(String originPlanet) {
		this.originPlanet = originPlanet;
	}
	public String getPictureUrl() {
		return pictureUrl;
	}
	public void setPictureUrl(String pictureUrl) {
		this.pictureUrl = pictureUrl;
	}
	
}
