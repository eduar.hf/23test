package net.people.demo.controller;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
class Person {

	private @Id @GeneratedValue Long id;
	private String nationalId;
	private String name;
	private String lastName;
	private Integer age;
	private String originPlanet;
	private String pictureUrl;

	Person() {}

	Person(String nationalId, String name, String lastName, Integer age, String originPlanet, String pictureUrl) {
		
		this.nationalId = nationalId;
		this.name = name;
		this.lastName = lastName;
		this.age = age;
		this.originPlanet = originPlanet;
		this.pictureUrl = pictureUrl;
		
	}

	public Long getId() {
		return this.id;
	}

	public String getName() {
		return this.name;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNationalId() {
		return nationalId;
	}

	public void setNationalId(String nationalId) {
		this.nationalId = nationalId;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getOriginPlanet() {
		return originPlanet;
	}

	public void setOriginPlanet(String originPlanet) {
		this.originPlanet = originPlanet;
	}

	public String getPictureUrl() {
		return pictureUrl;
	}

	public void setPictureUrl(String pictureUrl) {
		this.pictureUrl = pictureUrl;
	}

	@Override
	public boolean equals(Object o) {

		if (this == o)
			return true;
		if (!(o instanceof Person))
			return false;
		Person employee = (Person) o;
		return Objects.equals(this.id, employee.id) && Objects.equals(this.name, employee.name)
				&& Objects.equals(this.nationalId, employee.nationalId);
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.id, this.name, this.nationalId);
	}

	@Override
	public String toString() {
		return "Person{" + "id=" + this.id + ", name='" + this.name + '\'' + ", nationalId='" + this.nationalId + '\'' + '}';
	}
}
